package com.kmarinos.hellospring.resources;

import com.kmarinos.hellospring.statistics.StatisticsService;
import com.kmarinos.hellospring.statistics.model.Statistics;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@edu.umd.cs.findbugs.annotations.SuppressFBWarnings("ENTITY_LEAK")
@RestController
@RequestMapping("api/statistics")
@Slf4j
@RequiredArgsConstructor
public class StatisticsResource {

    private final StatisticsService statisticsService;

    @PostMapping
    public void logRequest(HttpServletRequest request){
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        final String sanitizedAddress=ipAddress.replaceAll("[\r\n]","");
        log.info("Request received from {}",sanitizedAddress);
    }
    @GetMapping
    public Statistics getCurrentStatistics(HttpServletRequest request){
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        final String sanitizedAddress=ipAddress.replaceAll("[\r\n]","");
        log.info("Request received from {}",sanitizedAddress);
        return statisticsService.getCurrentStatistics(ipAddress);
    }
    @GetMapping("/poll")
    public Map<String,String> pollCurrentStatistics(){
        return statisticsService.getVisitorStatistics();
    }
}
