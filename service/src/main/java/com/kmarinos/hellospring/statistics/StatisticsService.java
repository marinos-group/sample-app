package com.kmarinos.hellospring.statistics;

import com.kmarinos.hellospring.statistics.factories.StatisticsFactory;
import com.kmarinos.hellospring.statistics.model.Statistics;
import com.kmarinos.hellospring.statistics.repository.BuildInfoRepository;
import com.kmarinos.hellospring.statistics.repository.StatisticsRepository;
import com.kmarinos.hellospring.statistics.repository.SystemInfoRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class StatisticsService {

    private final StatisticsFactory statisticsFactory;
    private final StatisticsRepository statisticsRepository;
    private final SystemInfoRepository systemInfoRepository;
    private final BuildInfoRepository buildInfoRepository;

    private long totalVisitorsSinceRestart;
    @Getter
    private long totalVisitors;
    @PostConstruct
    public void init(){
        totalVisitors=statisticsRepository.count();
    }

    public Map<String,String> getVisitorStatistics(){
        return Map.of("visitorsSession", getRealTotalVisitorsSinceRestart(),"visitorsAllTime", getRealTotalVisitors());
    }
    public Statistics getCurrentStatistics(String fromAddress){
        totalVisitorsSinceRestart++;
        final var stats=statisticsFactory.createFromRequestAddress(fromAddress);
        stats.setVisitorsSession(getRealTotalVisitorsSinceRestart());
        stats.setVisitorsAllTime(getRealTotalVisitors());
        systemInfoRepository.save(stats.getSystemInfo());
        buildInfoRepository.save(stats.getBuildInfo());

        return statisticsRepository.save(stats);
    }
    private String getRealTotalVisitorsSinceRestart(){
        return Long.toString(totalVisitorsSinceRestart);
    }
    private String getRealTotalVisitors(){
        return Long.toString(totalVisitors+totalVisitorsSinceRestart);
    }
}
