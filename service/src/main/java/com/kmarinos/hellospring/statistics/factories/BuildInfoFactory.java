package com.kmarinos.hellospring.statistics.factories;

import com.kmarinos.hellospring.statistics.model.BuildInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.info.InfoEndpoint;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Slf4j
public class BuildInfoFactory {

    private final InfoEndpoint infoEndpoint;

    @SuppressWarnings("unchecked")
    public BuildInfo createFromActuatorInfo(){
        return this.createFromMaps((Map<String,Object>)infoEndpoint.info().get("git"),(Map<String,Object>)infoEndpoint.info().get("build"));
    }
    @SuppressWarnings("unchecked")
    private BuildInfo createFromMaps(Map<String,Object>git, Map<String,Object> build){
        final var builder=BuildInfo.builder();
        if (git != null) {
            builder.gitBranch(Optional.ofNullable(git.get("branch").toString()).orElse(""));
            final var commitKey = "commit";
            if(git.get(commitKey)!=null){
                builder.gitCommit(Optional.of(((Map<String,Object>)git.get(commitKey)).get("id").toString()).orElse(""));
                builder.commitDate(Optional.of(((Map<String,Object>)git.get(commitKey)).get("time").toString()).orElse(""));
            }
        }
        if(build!=null){
            builder.mavenArtifact(Optional.ofNullable(build.get("group").toString()).orElse("")+
                    "."+
                    Optional.ofNullable(build.get("artifact").toString()).orElse(""));
            builder.mavenVersion(Optional.ofNullable(build.get("version").toString()).orElse(""));
        }
        return builder.build();
    }
}
