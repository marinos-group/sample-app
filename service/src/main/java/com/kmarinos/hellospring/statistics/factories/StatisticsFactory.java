package com.kmarinos.hellospring.statistics.factories;

import com.kmarinos.hellospring.statistics.model.Statistics;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class StatisticsFactory {

    private final SystemInfoFactory systemInfoFactory;
    private final BuildInfoFactory buildInfoFactory;
    @Value("${spring.application.name:}")
    private String appName;
    @Value(("${build.version:}"))
    private String version;

public Statistics createFromRequestAddress(String requestAddress){
    log.info("System info {}:{}",appName,version);
    final var request= Statistics.builder()
            .fromAddress(requestAddress)
            .appName(appName)
            .version(version)
            .build();
    request.setSystemInfo(systemInfoFactory.createCurrent());
    request.setBuildInfo(buildInfoFactory.createFromActuatorInfo());
    return request;
}
}
