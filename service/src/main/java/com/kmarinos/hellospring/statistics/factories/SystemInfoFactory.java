package com.kmarinos.hellospring.statistics.factories;

import com.kmarinos.hellospring.statistics.model.SystemInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.env.EnvironmentEndpoint;
import org.springframework.boot.actuate.info.InfoEndpoint;
import org.springframework.boot.actuate.metrics.MetricsEndpoint;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.StringCharacterIterator;
import java.time.Duration;
import java.util.Collections;
import java.util.Locale;
import java.util.Optional;
import java.util.function.UnaryOperator;

@Component
@RequiredArgsConstructor
@Slf4j
public class SystemInfoFactory {

    private final EnvironmentEndpoint environmentEndpoint;
    private final MetricsEndpoint metricsEndpoint;
    private final InfoEndpoint infoEndpoint;

    public SystemInfo createCurrent() {
        return SystemInfo.builder()
                .serverIP(getHostIPAddress())
                .jvmMemoryUsed(getByteStatistic("jvm.memory.used"))
                .jvmHeapMax(getByteStatistic("jvm.memory.max", "area:heap"))
                .jvmHeapMin(getByteStatistic("jvm.memory.committed", "area:heap"))
                .port(getEndpointValue(environmentEndpoint.environmentEntry("server.port")))
                .localPath(getEndpointValue(environmentEndpoint.environmentEntry("user.dir")))
                .osName(getEndpointValue(environmentEndpoint.environmentEntry("os.name")))
                .cpuInfo(getEndpointValue(environmentEndpoint.environmentEntry("sun.cpu.isalist")))
                .appUser(getEndpointValue(environmentEndpoint.environmentEntry("user.name")))
                .jreVersion(getEndpointValue(environmentEndpoint.environmentEntry("java.runtime.version")))
                .javaVendorVersion(getEndpointValue(environmentEndpoint.environmentEntry("java.vendor.version")))
                .javaReleaseDate(getEndpointValue(environmentEndpoint.environmentEntry("java.version.date")))
                .javaHome(getEndpointValue(environmentEndpoint.environmentEntry("java.home")))
                .timezone(getEndpointValue(environmentEndpoint.environmentEntry("user.timezone")))
                .pid(getEndpointValue(environmentEndpoint.environmentEntry("PID")))
                .cpuUsagePercent(getEndpointValue(metricsEndpoint.metric("process.cpu.usage", null), 2, v -> 100 * v) + "%")
                .serviceUptime(humanReadableDuration(getEndpointValue(metricsEndpoint.metric("process.uptime", null))))
                .build();
    }

    private String humanReadableDuration(String seconds) {
        var returnValue = "";
        if (seconds != null && !seconds.isEmpty()) {
            returnValue = Duration.ofSeconds(Long.parseLong(seconds)).toString()
                    .substring(2)
                    .replaceAll("(\\d[HMS])(?!$)", "$1 ")
                    .toLowerCase(Locale.ROOT);
        }
        return returnValue;
    }

    private String getByteStatistic(String name) {
        return toHumanReadableBytes(
                getEndpointValue(metricsEndpoint.metric(name, null))
        );
    }

    private String getByteStatistic(String name, String tag) {
        return toHumanReadableBytes(
                getEndpointValue(metricsEndpoint.metric(name, Collections.singletonList(tag)))
        );
    }

    private String getHostIPAddress() {
        final String hostName = getEndpointValue(environmentEndpoint.environmentEntry("USERDOMAIN"));
        var returnValue = "";
        try {
            if (!hostName.isEmpty()) {
                returnValue = String.valueOf(InetAddress.getByName(hostName).getHostAddress());
            }
        } catch (UnknownHostException e) {
            try {
                returnValue = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException ex) {
                log.debug("No ip address could be found. Empty String is returned.");
            }
        }
        return returnValue;
    }


    private String getEndpointValue(MetricsEndpoint.MetricResponse metricResponse, int decimalPlaces, UnaryOperator<Double> transform) {
        final var response = Optional.ofNullable(metricResponse);
        final var sb = new StringBuilder();
        sb.append("###");
        if (decimalPlaces > 0) {
            sb.append('.');
        }
        sb.append("#".repeat(Math.max(0, decimalPlaces)));
        UnaryOperator<Double> operator = v -> v;
        if (transform != null) {
            operator = transform;
        }
        return response.map(value -> value.getMeasurements().get(0).getValue()).map(operator).map(value -> new DecimalFormat(sb.toString()).format(value)).orElse("");
    }

    private String getEndpointValue(final MetricsEndpoint.MetricResponse metricResponse) {
        final var response = Optional.ofNullable(metricResponse);

        return response.map(this::getMetricValueFormatted).orElse("");
    }

    private String getMetricValueFormatted(final MetricsEndpoint.MetricResponse response) {
        final var measurements = response.getMeasurements();
        final var firstMeasurement = measurements.get(0);
        final var value = firstMeasurement.getValue();
        return new DecimalFormat("###").format(value);
    }

    private String getEndpointValue(EnvironmentEndpoint.EnvironmentEntryDescriptor descriptor) {
        final var param = Optional.ofNullable(descriptor.getProperty());
        String returnValue = "";
        if (param.isPresent()) {
            returnValue = param.get().getValue().toString();
        }
        return returnValue;
    }

    private static String toHumanReadableBytes(String bytesString) {
        final var bytes = Long.parseLong(bytesString);
        final var absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        String returnValue;
        if (absB < 1024) {
            returnValue = bytes + " B";
        }else{
            long value = absB;
            final var ci = new StringCharacterIterator("KMGTPE");
            for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
                value >>= 10;
                ci.next();
            }
            value *= Long.signum(bytes);
            returnValue = String.format("%.1f %ciB", value / 1024.0, ci.current());
        }
        return returnValue;
    }
}
