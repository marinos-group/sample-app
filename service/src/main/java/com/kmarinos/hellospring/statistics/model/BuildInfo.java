package com.kmarinos.hellospring.statistics.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Map;
import java.util.Optional;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BuildInfo {
    @Id
    @GeneratedValue
    Long id;

    @JsonProperty("Maven artifact")
    String mavenArtifact;

    @JsonProperty("Maven artifact version")
    String mavenVersion;

    @JsonProperty("Git Branch")
    String gitBranch;

    @JsonProperty("Git Commit")
    String gitCommit;

    @JsonProperty("Committed at")
    String commitDate;






    @SuppressWarnings({"unchecked"})
    public BuildInfo(Map<String, Object> git, Map<String, Object> build) {
        if (git != null) {
            this.gitBranch= Optional.ofNullable(git.get("branch").toString()).orElse("");
            final var commitKey = "commit";
            if(git.get(commitKey)!=null){
                this.gitCommit= Optional.of(((Map<String,Object>)git.get(commitKey)).get("id").toString()).orElse("");
                this.commitDate= Optional.of(((Map<String,Object>)git.get(commitKey)).get("time").toString()).orElse("");
            }
        }
        if(build!=null){
            this.mavenArtifact= Optional.ofNullable(build.get("group").toString()).orElse("")+
                    "."+
                    Optional.ofNullable(build.get("artifact").toString()).orElse("");
            this.mavenVersion=Optional.ofNullable(build.get("version").toString()).orElse("");
        }
    }
}
