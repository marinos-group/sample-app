package com.kmarinos.hellospring.statistics.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Statistics {
    @Id
    @GeneratedValue
    Long id;
    LocalDateTime createdAt;
    String fromAddress;
    String appName;
    String version;
    String visitorsSession;
    String visitorsAllTime;
    @OneToOne
    SystemInfo systemInfo;
    @OneToOne
    BuildInfo buildInfo;

    @Builder
    public Statistics(String fromAddress, String appName, String version, String visitorsSession, String visitorsAllTime) {
        this.fromAddress = fromAddress;
        this.appName=appName;
        this.version=version;
        this.visitorsSession=visitorsSession;
        this.visitorsAllTime=visitorsAllTime;
        this.createdAt = LocalDateTime.now();
    }
}


