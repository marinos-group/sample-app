package com.kmarinos.hellospring.statistics.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SystemInfo {
    @Id
    @GeneratedValue
    Long id;

    @JsonProperty("Port")
    String port;
    @JsonProperty("Server IP address")
    String serverIP;
    @JsonProperty("Memory used (JVM)")
    String jvmMemoryUsed;
    @JsonProperty("Heap Max (JVM)")
    String jvmHeapMax;
    @JsonProperty("Heap Min (JVM)")
    String jvmHeapMin;
    @JsonProperty("Installed at")
    String localPath;
    @JsonProperty("Operating system")
    String osName;
    @JsonProperty("CPU Architecture")
    String cpuInfo;
    @JsonProperty("Application User")
    String appUser;
    @JsonProperty("Java Runtime Version")
    String jreVersion;
    @JsonProperty("Java Vendor Version")
    String javaVendorVersion;
    @JsonProperty("Java Version Release Date")
    String javaReleaseDate;
    @JsonProperty("Java Home")
    String javaHome;
    @JsonProperty("Server Timezone")
    String timezone;
    @JsonProperty("PID")
    String pid;
    @JsonProperty("CPU Usage")
    String cpuUsagePercent;
    @JsonProperty("Service Uptime")
    String serviceUptime;

}
