package com.kmarinos.hellospring.statistics.repository;

import com.kmarinos.hellospring.statistics.model.BuildInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BuildInfoRepository extends JpaRepository<BuildInfo,Long> {
}
