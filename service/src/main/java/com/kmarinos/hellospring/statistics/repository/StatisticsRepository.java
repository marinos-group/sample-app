package com.kmarinos.hellospring.statistics.repository;

import com.kmarinos.hellospring.statistics.model.Statistics;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatisticsRepository extends JpaRepository<Statistics,Long> {
}
