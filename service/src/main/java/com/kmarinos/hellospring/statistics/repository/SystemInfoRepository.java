package com.kmarinos.hellospring.statistics.repository;

import com.kmarinos.hellospring.statistics.model.SystemInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SystemInfoRepository extends JpaRepository<SystemInfo,Long> {
}
