package com.kmarinos.hellospring.resource;

import com.kmarinos.hellospring.SampleApplication;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/*@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SampleApplication.class)
@AutoConfigureMockMvc*/
//@AutoConfigureTestDatabase
class SampleAppResourceTests {
    /* @Autowired
    MockMvc mvc;
    private final static String BASE_URL = "/api";

    @Test
    void given_nothing_should_return_message() throws Exception {
       String reply= mvc.perform(get(BASE_URL+"/hello")).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
       assertThat(reply).isEqualTo("Hello world");
    }
    @Test
    void given_string_should_return_parametrized_message() throws Exception{
        String message="user";
        String reply= mvc.perform(get(BASE_URL+"/hello-message?q="+message)).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        assertThat(reply).isEqualTo("Hello user");
    }
    @Test
    void given_missing_string_should_return_default_parametrized_message() throws Exception{
        String reply= mvc.perform(get(BASE_URL+"/hello-message")).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        assertThat(reply).isEqualTo("Hello unknown");
    }*/
}
