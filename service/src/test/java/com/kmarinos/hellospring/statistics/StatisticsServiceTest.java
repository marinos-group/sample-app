package com.kmarinos.hellospring.statistics;

import com.kmarinos.hellospring.statistics.factories.StatisticsFactory;
import com.kmarinos.hellospring.statistics.model.BuildInfo;
import com.kmarinos.hellospring.statistics.model.Statistics;
import com.kmarinos.hellospring.statistics.model.SystemInfo;
import com.kmarinos.hellospring.statistics.repository.BuildInfoRepository;
import com.kmarinos.hellospring.statistics.repository.StatisticsRepository;
import com.kmarinos.hellospring.statistics.repository.SystemInfoRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class StatisticsServiceTest {

    @InjectMocks
    StatisticsService statisticsService;
    @Mock
    StatisticsRepository statisticsRepository;
    @Mock
    StatisticsFactory statisticsFactory;
    @Mock
    SystemInfoRepository systemInfoRepository;
    @Mock
    BuildInfoRepository buildInfoRepository;

    @Test
    public void given_nothing_visitors_should_initialize(){
        Mockito.when(statisticsRepository.count()).thenReturn(10L);
        statisticsService.init();

        assertThat(statisticsService.getTotalVisitors()).isEqualTo(10L);
    }
    @Test
    public void given_initial_and_session_visitors_should_return_correct_visitors(){
        ReflectionTestUtils.setField(statisticsService,StatisticsService.class,"totalVisitorsSinceRestart",5L,null);
        ReflectionTestUtils.setField(statisticsService,StatisticsService.class,"totalVisitors",10L,null);

        assertThat(statisticsService.getVisitorStatistics()).isEqualTo(Map.of("visitorsSession", "5","visitorsAllTime", "15"));
    }
    @Test
    public void given_from_address_should_return_statistics(){
        ReflectionTestUtils.setField(statisticsService,StatisticsService.class,"totalVisitorsSinceRestart",5L,null);
        ReflectionTestUtils.setField(statisticsService,StatisticsService.class,"totalVisitors",10L,null);
        String fromAddress="1.1.1.1";

        Mockito.when(statisticsFactory.createFromRequestAddress(fromAddress))
                .thenReturn(Statistics.builder().fromAddress(fromAddress).build());
        Mockito.when(systemInfoRepository.save(Mockito.any(SystemInfo.class)))
                .thenAnswer(i->i.getArguments()[0]);
        Mockito.when(buildInfoRepository.save(Mockito.any(BuildInfo.class)))
                .thenAnswer(i->i.getArguments()[0]);
        Mockito.when(statisticsRepository.save(Mockito.any(Statistics.class)))
                .thenAnswer(i->i.getArguments()[0]);
        Statistics toTest=statisticsService.getCurrentStatistics(fromAddress);
        assertThat(toTest.getFromAddress()).isEqualTo(fromAddress);
        assertThat(toTest.getVisitorsAllTime()).isEqualTo("16");
        assertThat(toTest.getVisitorsSession()).isEqualTo("6");

    }
}
