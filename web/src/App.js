import _ from "lodash";
import { useEffect, useState } from "react";
import CardWithTable from "./components/CardWithTable";
import AppHeader from "./features/AppHeader";
import useInterval from "./hooks/useInterval";

function App() {
  const [statistics, setStatistics] = useState({});
  const [visitorsInfo, setVisitorsInfo] = useState({});
  const pollingInterval = 5000;
  useEffect(() => {
    fetchStatistics();
  }, []);
  useInterval(() => {
    pollVisitors();
  }, pollingInterval);
  const pollVisitors = () => {
    fetch("/api/statistics/poll", {
      headers: {
        Accept: "application/json",
      },
    })
      .then((response) => response.json())
      .then((r) => {
        if (visitorsInfo && visitorsInfo.session && visitorsInfo.total) {
          setVisitorsInfo({
            session: {
              initial: visitorsInfo.session.actual,
              actual: r.visitorsSession,
            },
            total: {
              initial: visitorsInfo.total.actual,
              actual: r.visitorsAllTime,
            },
          });
        } else {
          fetchStatistics();
        }
      })
      .catch((error) => {
        setVisitorsInfo({
          initial: 0,
          actual: 0,
        });
      });
  };
  const fetchStatistics = () => {
    fetch("/api/statistics", {
      headers: {
        Accept: "application/json",
      },
    })
      .then((response) => response.json())
      .then((r) => {
        setStatistics(r);
        setVisitorsInfo({
          session: {
            initial: r.visitorsSession,
            actual: r.visitorsSession,
          },
          total: {
            initial: r.visitorsAllTime,
            actual: r.visitorsAllTime,
          },
        });
      })
      .catch(() => {
        /* ignore */
      });
  };
  return (
    <div className="container mx-auto lg:max-w-4xl ">
      <AppHeader
        appName={statistics.appName}
        version={statistics.version}
        visitors={visitorsInfo.session}
        visitorsTotal={visitorsInfo.total}
        animationDuration={pollingInterval}
      />
      <div className="my-4">
        <CardWithTable
          title="System Info"
          color="blue"
          data={_.omit(statistics.systemInfo, "id")}
        />
      </div>
      <div className="my-4">
        <CardWithTable
          title="Build Info"
          color="blue"
          data={_.omit(statistics.buildInfo, "id")}
        ></CardWithTable>
      </div>
    </div>
  );
}

export default App;
