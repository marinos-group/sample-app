export default function Card({ children, title, titleColor }) {
  const titleColorTWClass = () => {
    switch (titleColor) {
      case "blue":
        return "bg-[#3e92ea] ";
      case "green":
        return "bg-green-700 ";
      case "red":
        return "bg-red-700 ";
      case "orange":
        return "bg-yellow-700 ";
      case "gray":
        return "bg-gray-700 ";
      default:
        return "bg-blue-700 ";
    }
  };

  return (
    <div className="container shadow-md overflow-auto">
      <div className={` flex h-12 px-2 text-white ${titleColorTWClass()}`}>
        <div className="my-1 text-2xl">{title}</div>
      </div>
      <div className="">{children}</div>
    </div>
  );
}
