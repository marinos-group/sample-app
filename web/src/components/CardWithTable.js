import Card from "./Card";
import KeyValuePairTable from "./KeyValuePairTable";

export default function CardWithTable({ title, data, color = "blue" }) {
  return (
    <Card title={title} titleColor={color}>
      <KeyValuePairTable data={data}></KeyValuePairTable>
    </Card>
  );
}
