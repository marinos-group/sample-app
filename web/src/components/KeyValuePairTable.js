export default function KeyValuePairTable({ data }) {
  const renderTable = () => {
    if (!data) {
      return null;
    }
    return Object.entries(data).map(([key, value], idx) => {
      return (
        <div
          key={key}
          className={`${idx % 2 === 1 ? "bg-gray-200" : ""} py-2 flex flex-row`}
        >
          <div className="max-w-50 px-2 min-w-30 ">{key}</div>

          <div className="px-2">{value}</div>
        </div>
      );
    });
  };
  return <div className="">{renderTable()}</div>;
}
