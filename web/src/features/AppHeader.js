import AnimatedNumber from "react-animated-number/build/AnimatedNumber";

export default function AppHeader({
  appName,
  version,
  visitors,
  visitorsTotal,
  animationDuration = 2000,
}) {
  const renderAppInfo = () => {
    const renderVisitors = (visitors) => {
      if (!visitors) {
        return "...";
      }
      return (
        <AnimatedNumber
          initialValue={parseInt(visitors.initial)}
          value={parseInt(visitors.actual)}
          duration={animationDuration + 300}
          formatValue={(n) => parseInt(n).toFixed(0)}
        />
      );
    };
    return (
      <div className="flex flex-column">
        <div>
          <div className="px-2">Version</div>
          <div className="px-2">Visitors (since last restart)</div>
          <div className="px-2">Visitors (total)</div>
        </div>
        <div>
          <div className="px-2 text-left font-bold">{version}</div>
          <div className="px-2 text-left font-bold">
            {renderVisitors(visitors)}
          </div>
          <div className="px-2 text-left font-bold">
            {renderVisitors(visitorsTotal)}
          </div>
        </div>
      </div>
    );
  };
  return (
    <div className="h-auto sm:pt-4 flex flex-column flex-wrap sm:flex-row justify-between">
      <h1 className="text-4xl font-semibold h-1/2 m-auto sm:my-auto sm:mx-0">
        {appName}
      </h1>
      <div className="h-3/4 m-auto sm:my-auto sm:mx-0 font-semibold">
        {renderAppInfo()}
        {/*renderSecondaryInfo({
          Version: version,
          "Visitors (since last restart)": visitors,
          "Visitors (total)": visitorsTotal,
        })*/}
      </div>
    </div>
  );
}
